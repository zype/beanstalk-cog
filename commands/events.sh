#!/bin/bash

. ./shared_functions.sh
start
if [[ ${COG_ARGV_1:?} ]]; then
    eb events $COG_ARGV_1 | tail -n 20
else
    echo "No Beanstalk environment specified."
fi
end