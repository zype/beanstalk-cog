#!/bin/bash

. ./shared_functions.sh
start
if [[ ${COG_ARGV_1:?} ]]; then
    eb health --mono $COG_ARGV_1
else
    echo "No Beanstalk environment specified."
fi
end