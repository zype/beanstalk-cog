#!/bin/bash

declare -a arguments

echo "COG_TEMPLATE: beanstalk"
export PAGER=tee

echoerr() { echo "$@" 1>&2; }

test_for_ssh_key () {
    if [ "$EB_SSH_KEY" == "" ]; then
        echoerr "No EB_SSH_KEY provided."
        exit 1
    else
        echo -e "$EB_SSH_KEY" > ~/.ssh/id_rsa
        chmod 0600 ~/.ssh/id_rsa
    fi
}

test_for_aws_keys () {
    if [ "$AWS_ACCESS_KEY_ID" == "" ]; then
        echoerr "No AWS_ACCESS_KEY_ID specified."
        exit 1
    fi

    if [ "$AWS_SECRET_ACCESS_KEY" == "" ]; then
        echoerr "No AWS_SECRET_ACCESS_KEY specified."
        exit 1
    fi
}

test_for_git_config () {
    if [ "$GIT_HOST" == "" ]; then
        echoerr "No GIT_HOST set."
        exit 1
    fi

    if [ "$GIT_ORG" == "" ]; then
        echoerr "No GIT_ORG set."
        exit 1
    fi
}

clone_repo () {
    if [ "$COG_ARGV_0" == "" ] ; then
        echoerr "Missing the GIT repository Name."
        exit 1
    fi

    if [ ! -d "$COG_ARGV_0" ]; then
        GIT_URL=$(echo -e "$GIT_HOST:$GIT_ORG/$COG_ARGV_0.git")
        echo -e "COGCMD_INFO: Cloning $GIT_URL"
        git clone -q $GIT_URL
    else
        cd $COG_ARGV_0
        git pull
        cd ..
    fi

    if [ ! -d "$COG_ARGV_0" ]; then
        echoerr "Error cloning $COG_ARGV_0."
        exit 1
    else
        cd $COG_ARGV_0
    fi
}

start () {
    test_for_ssh_key
    test_for_aws_keys
    test_for_git_config
    clone_repo
}

end () {
    cd ~
}
