#!/bin/bash

. ./shared_functions.sh

start

if [[ ${COG_ARGV_2:?} ]]; then
    git checkout $COG_ARGV_2
    git pull origin $COG_ARGV_2
fi

if [[ ${COG_ARGV_1:?} ]]; then
    echo "COG_TEMPLATE: beanstalkdeploy"
    echo "COGCMD_INFO: Deploying $(git branch | grep \*) to $COG_ARGV_1"
    eb deploy -nh $COG_ARGV_1
else
    echoerr "No Beanstalk environment specified."
    exit 1
fi
end
