#!/bin/bash

. ./shared_functions.sh
start

if [ $LOG_BUCKET == "" ]; then
    echoerr "LOG_BUCKET not specified."
    exit 1
fi

if [[ ${COG_ARGV_1:?} ]]; then
    echo "COG_TEMPLATE: beanstalklogs"
    eb logs --zip --quiet $COG_ARGV_1
    FILENAME=$(ls .elasticbeanstalk/logs)
    aws s3 cp --quiet .elasticbeanstalk/logs/$FILENAME s3://$LOG_BUCKET/
    echo -e "Logs are available at $(aws s3 presign --expires-in 28800 s3://$LOG_BUCKET/$FILENAME)"
else
    echo "No Beanstalk environment specified."
fi
end