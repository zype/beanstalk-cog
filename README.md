# Cog Beanstalk Commands

## Required config parameters

- `AWS_ACCESS_KEY_ID` - AWS API Key which has the ability to deploy to and query Beanstalk
- `AWS_SECRET_ACCESS_KEY` - AWS API Secret Key which has the ability to deploy to and query Beanstalk
- `EB_SSH_KEY` - a SSH private key which has the ability to clone from the repositories you're working with
- `GIT_HOST` - Git URL host such as `git@github.com`
- `GIT_ORG` - Organization or user which houses the repositories you're working with such as `zype`
- `LOG_BUCKET` - S3 bucket name to put log files