FROM alpine:3.6
RUN apk add --no-cache python2 py2-pip git bash openssh-client && \
    pip install awsebcli && \
    pip install awscli && \
    adduser ebuser -s /bin/bash -D
COPY commands/ /home/ebuser/
RUN chown -R ebuser:ebuser /home/ebuser
USER ebuser
WORKDIR /home/ebuser
RUN mkdir ~/.ssh && \
    chmod 0700 ~/.ssh && \
    touch ~/.ssh/id_rsa && \
    touch ~/.ssh/known_hosts && \
    chmod 0600 ~/.ssh/known_hosts && \
    chmod 0600 ~/.ssh/id_rsa && \
    chmod +x ./*.sh
RUN if [ ! -n "$(grep "^github.com " ~/.ssh/known_hosts)" ]; then ssh-keyscan github.com >> ~/.ssh/known_hosts 2>/dev/null; fi
RUN if [ ! -n "$(grep "^gitlab.com " ~/.ssh/known_hosts)" ]; then ssh-keyscan gitlab.com >> ~/.ssh/known_hosts 2>/dev/null; fi